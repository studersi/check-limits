use std::fmt::Debug;
use std::process;
use std::process::Command;
use clap::Parser;
use rlimit::{Resource, setrlimit};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// command to run after printing limits
    #[arg(last = true)]
    exec: Vec<String>,
}

const LIMITS: [Resource; 26] = [
    Resource::AS,
    Resource::CORE,
    Resource::CPU,
    Resource::DATA,
    Resource::FSIZE,
    Resource::KQUEUES,
    Resource::LOCKS,
    Resource::MEMLOCK,
    Resource::MSGQUEUE,
    Resource::NICE,
    Resource::NOFILE,
    Resource::NOVMON,
    Resource::NPROC,
    Resource::NPTS,
    Resource::NTHR,
    Resource::POSIXLOCKS,
    Resource::RSS,
    Resource::RTPRIO,
    Resource::RTTIME,
    Resource::SBSIZE,
    Resource::SIGPENDING,
    Resource::STACK,
    Resource::SWAP,
    Resource::THREADS,
    Resource::UMTXP,
    Resource::VMEM,
];

fn main() {
    let args = Args::parse();

    println!();

    let process_id = process::id();
    println!("The PID of this process is: {process_id}");

    println!();

    println!("Printing the contents of /proc/{process_id}/limits ...");
    Command::new("cat")
        .arg(format!("/proc/{process_id}/limits"))
        .status().expect("failed to execute process");

    println!();

    println!("Printing information for all limits:");
    println!("{:?}", LIMITS);

    println!();

    for limit in LIMITS {
        let limit_name = limit.as_name();
        match limit.is_supported() {
            true => {
                println!("{limit_name}: supported");

                let limit_soft = limit.get_soft().unwrap();
                let limit_hard = limit.get_hard().unwrap();
                let limit_soft_is_infinity = limit.get_soft().unwrap() == rlimit::INFINITY;
                let limit_hard_is_infinity = limit.get_hard().unwrap() == rlimit::INFINITY;
                println!("{limit_name}: soft={limit_soft} (unlimited={limit_soft_is_infinity}), hard={limit_hard} (unlimited={limit_hard_is_infinity})");

                match limit_soft < limit_hard {
                    true => {
                        println!("{limit_name}: soft limit ({limit_soft}) is smaller than hard limit ({limit_hard}), trying to increase it");
                        match setrlimit(limit, limit_hard, limit_hard) {
                            Ok(_) => {
                                println!("{limit_name}: successfully increased soft limit to value of hard limit");
                            }
                            Err(err) => {
                                println!("{limit_name}: could not increase soft limit to value of hard limit: {err:?}");
                            }
                        }
                    }
                    false => {
                        println!("{limit_name}: soft limit is not smaller than hard limit");
                    }
                }

                match limit_soft < rlimit::INFINITY || limit_hard < rlimit::INFINITY {
                    true => {
                        println!("{limit_name}: soft limit or hard limit are smaller than infinity ({}), trying to increase it", rlimit::INFINITY);
                        match setrlimit(limit, rlimit::INFINITY, rlimit::INFINITY) {
                            Ok(_) => {
                                println!("{limit_name}: successfully increased soft limit and hard limit to value of infinity");
                            }
                            Err(_) => {
                                println!("{limit_name}: could not increase soft limit or hard limit to value of infinity");
                            }
                        }
                    }
                    false => {
                        println!("{limit_name}: soft limit and hard limit are not smaller than infinity");
                    }
                }

                let limit_soft = limit.get_soft().unwrap();
                let limit_hard = limit.get_hard().unwrap();
                let limit_soft_is_infinity = limit.get_soft().unwrap() == rlimit::INFINITY;
                let limit_hard_is_infinity = limit.get_hard().unwrap() == rlimit::INFINITY;
                println!("{limit_name}: soft={limit_soft} (unlimited={limit_soft_is_infinity}), hard={limit_hard} (unlimited={limit_hard_is_infinity})");
            }
            false => {
                println!("{limit_name}: not supported");
            }
        };
        println!();
    }

    if args.exec.len() > 0 {
        eprintln!("Running command: {:?}", args.exec);
        let mut cmd = Command::new(&args.exec[0]);
        if args.exec.len() > 1 {
            cmd.args(&args.exec[1..]);
        }
        cmd.spawn().unwrap().wait().unwrap();
    }
}
